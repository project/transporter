<?php

/**
 * Process incoming request and redirect.
 */
function transporter_process($incoming_path_array = array()) {
	global $db_url;
  
	if (!is_array($db_url)) {
		// Module not configured in settings.php
		/**
		 * @todo set error message and don't proceed
		 */
	}

  // Source of request.
	$incoming_key = array_shift($incoming_path_array);	

  // If no path, redirect to homepage.
	if (empty($incoming_path_array)) {
		drupal_goto($path = '', $query = NULL, $fragment = NULL, $http_response_code = 301);
	}
	
	$master_site = variable_get('transporter_master_site', '');
  $is_master_site = variable_get('transporter_is_master_site', TRUE);
  
	// If not on master site switch db.
  if (!$is_master_site) {
	  db_set_active($master_site);
  }
	
  // Match source path to a target path, or take a guess.
  $redirect_path = transporter_target_get($incoming_key, $incoming_path_array); 
  
  // Match based on paths entered as matches.
	if (empty($redirect_path)) {
    $redirect_path = transporter_target_get($incoming_key, $incoming_path_array, $recursive = TRUE); 
	}

  // Switch back to usual db. 
  if (!$is_master_site) {
	  db_set_active('default');
	}

  // Match based on paths that exist on the site.
	if (empty($redirect_path)) {
	  $redirect_path = transporter_path_match($incoming_path_array);
  }

	// Avoid possibility of an infinite redirect loop.
	if ($redirect_path == $_GET["q"]) {
		return;
	}

  // Redirect to proper homepage instead of /node
	if ($redirect_path == 'node') {
		$redirect_path = '';
	}

  drupal_goto($redirect_path, $query = NULL, $fragment = NULL, $http_response_code = 301);	
}


/**
 * Match source path to target path, or take a guess if no direct match.
 */
function transporter_target_get($key, $source_path_array, $recursive = FALSE) {

	$source = implode('/', $source_path_array);

	// Get master target so we can get all related paths.
	$target = db_result(db_query_range("SELECT target FROM {transporter_paths} WHERE source_key = '%s' AND source = '%s'", $key, $source, 0, 1));

	// Find a match by removing elements of the path.
	if (empty($target) && $recursive == TRUE && count($source_path_array) > 0) {
    array_pop($source_path_array);
	  $target = transporter_target_get($key, $source_path_array, $recursive = TRUE);	
	}

	return $target; 
}

/**
 * Match source path to valid site path.
 */
function transporter_path_match($source_path_array) {
  global $base_url;
	$path = implode('/', $source_path_array);
  $path = drupal_get_normal_path($path);

	// Find a match by removing elements of the path. 
	if (!menu_valid_path(array('link_path' => $path)) && count($source_path_array) > 0) {
    array_pop($source_path_array);
	  $path = transporter_path_match($source_path_array);
	}

	return $path; 
}





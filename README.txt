********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Transporter Module 
Author: Robert Castelo
Sponsor: Code Positive [www.codepositive.com]
Drupal: 6.0.x
********************************************************************
DESCRIPTION:

    Provides links on one site that point to a specific page on another site.

    A common user case is that you run example.com (US), example.co.jp (Japanese), and 
    example.co.de (German). You want to have links at the top of each page of these sites pointing 
    to the equivalent page on the other sites.

   This module enables you to match pages across sites, and in the case where there is no match 
   take a best guess at a match.

********************************************************************
INSTALLATION:

    Note: It is assumed that you have Drupal up and running.  Be sure to
    check the Drupal web site if you need assistance.  If you run into
    problems, you should always read the INSTALL.txt that comes with the
    Drupal package and read the online documentation. 

	1. Place the entire module directory into your Drupal
        modules/directory.

	2. Enable the module by navigating to:

	   Administer > Site building > Modules

	Click the 'Save configuration' button at the bottom to commit your
        changes.


  *****************
  *** IMPORTANT ***
  *****************

  Set the $db_url in settings.php to be an array rather than a string,
  with a unique key for each site - these keys are also used later on in 
  configuration.

  For example:

  $db_url['default'] = 'mysql://username:password@localhost/databasename';
  $db_url['international'] = 'mysql://username:password@localhost/databasename';
  $db_url['uk'] = 'mysql://username:password@localhost/databasename_uk';
  $db_url['japan'] = 'mysql://username:password@localhost/databasename_japan';

  Note: This must be done for each site on the network

  Note: ['default'] must be set to the database of the site the settings.php file is for

  Note: ['default'] must also have another matched item to identify the database by, e.g.
        ['international'] (['default'] will be different on each site, the rest the same)


********************************************************************
CONFIGURATION

1] Go to the admin page: 
   'Content management' -> 'Inter Site Links' -> 'Configuration'

   /admin/content/transporter/configure

  'Key' must be the same keys used in the database array in settings.php

  'Label' will be what is presented to users in selects.

  'Domain' is the full domain of the site, for example:

   http://www.example.com

   'Active' enables the site to be displayed to the end user. 
   Sometimes this is useful to hide sites until they are ready.

   'Master Site Key' choose one of the sites to be your canonical data source.
   This site will hold all the information you record about the sites, e.g. path matches
   so make sure it is always in place.

2] Set permissions for transporter module

   admin/user/permissions

3] Add the block 'Transporter Page Settings' to your pages.
 
   This block enables pages to be matched across sites

4] Add the block 'Transporter Links' to your pages

   This block displays a drop down selector of sites to end users.

5] Go to pages and click on the 'Configure links to other sites' link.

   Fill in matching paths.

6] Administrate paths across the site at this page:

   /admin/content/transporter


********************************************************************
TROUBLESHOOTING
       
'Domain' must match the global $base_url for each site - for instance 
if you set Domain to http://example.com and the $base_url is http://www.example.com
this will cause issues.      
       
********************************************************************
ACKNOWLEDGEMENTS







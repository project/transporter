<?php

/**
 * Administration for all paths on the site. 
 */
function transporter_administration_add_paths_form(&$form_state) {
	$form = array();
	drupal_set_title('Transporter: Add New Paths');
  
  // New paths.
  $sites = variable_get('transporter_sites', array());

	$form['new_paths'] = array(
	  '#tree' => TRUE,
	);

	for ($i = 0; $i < 15; $i++) { 

		foreach ((array) $sites as $site) {
	    $key = $site['key'];

			$form['new_paths']['paths'][$i][$key] = array(
			  '#type' => 'textfield',
			  '#size' => 40,
			  '#maxlength' => 255,
			);
		}
	}
	
	$form['new_paths']['save_new'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save'),
	);
		
  return $form;	
}

function transporter_administration_add_paths_form_submit($form, &$form_state) {
  transporter_all_paths_set(array(), $form_state['values']['new_paths']['paths']);
  drupal_set_message(t('New paths added'));   	
}

function transporter_administration_edit_paths() {
  $content = '';
	$master_paths = transporter_all_paths_get();
	$content .= drupal_get_form('transporter_administration_edit_paths_form', $master_paths);
	$content .= theme('pager');
	return $content;
}

/**
 * Administration for all paths on the site. 
 */
function transporter_administration_edit_paths_form(&$form_state, $master_paths) {
	$form = array();
	drupal_set_title('Transporter: Edit Paths');
  $sites = transporter_sites_get();
 
	$form['paths'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Edit Paths'),
	  '#tree' => TRUE,
	);

	$form['paths_old'] = array(
	  '#tree' => TRUE,
	);

	foreach ($master_paths as $key => $paths) {

     $all_sites = $sites;

		foreach($paths as $path) {
			$form['paths'][$key][$path['source_key']] = array(
			  '#type' => 'textfield',
			  '#default_value' => $path['source'],
			  '#size' => 40,
			  '#maxlength' => 255,
			);
			// Provides info for updating of existing paths
			$form['paths_old'][$key][$path['source_key']] = array(
			  '#type' => 'value',
			  '#value' => $path['source'],
			);
			
			unset($all_sites[$path['source_key']]);
		}
		
		foreach ($all_sites as $source_key => $label) {
			$form['paths'][$key][$source_key] = array(
			  '#type' => 'textfield',
			  '#default_value' => '',
			  '#size' => 40,
			  '#maxlength' => 255,
			);
		} 
	}
	
	if (!empty($master_paths)) {
		$form['save'] = array(
		  '#type' => 'submit',
		  '#value' => t('Save'),
		);
	}
		
  return $form;	
}

function transporter_administration_edit_paths_form_submit($form, &$form_state) {  
  transporter_all_paths_set($form_state['values']['paths_old'], $form_state['values']['paths']);
  drupal_set_message(t('Paths updated.'));	
}
/**
 * Configure info about the sites.
 */
function transporter_site_configure_form(&$form_state) {
	$form = array();

  // Get sites list and settings for this page.		
	$sites = variable_get('transporter_sites', array());
	$master_site = variable_get('transporter_master_site', '');
	$note = variable_get('transporter_selector_note', t('Please note - you will no longer be logged in when viewing these other websites.')); 
	
	$form['settings'] = array(
	  '#tree' => TRUE,
	);
	
	foreach ((array) $sites as $site) {
		transporter_site_configure_fields($form, $site['key'], $site['label'], $site['domain'], $site['active']);
	}
	
  transporter_site_configure_fields($form);
  transporter_site_configure_fields($form);

	$form['master_site'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Master Site Key'),
	  '#description' => t('Key of master site, where all data will be stored. Must be same as key to database URL.'),
	  '#default_value' => isset($master_site) ? $master_site : '',
	  '#required' => TRUE,
	  '#size' => 5,
	  '#maxlength' => 25,
	);
	
	$form['selector_note'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Note'),
	  '#description' => t('Note to add to bottom of the selector warning authenticated user they will no longer be logged in on the other sites.'),
	  '#default_value' => $note,
	  '#required' => TRUE,
	  '#size' => 75,
	);

	$form['save'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save'),
	);

  return $form;	
}

function transporter_site_configure_fields(&$form, $key = '', $label = '', $domain = '', $active = 0) {

	static $row = 0;
	
	$form['settings'][$row]['key'] = array(
	  '#type' => 'textfield',
	  '#default_value' => isset($key) ? $key : '',
	  '#size' => 10,
	  '#maxlength' => 25,
	);

	$form['settings'][$row]['label'] = array(
	  '#type' => 'textfield',
	  '#default_value' => isset($label) ? $label : '',
	  '#size' => 15,
	  '#maxlength' => 255,
	);

	$form['settings'][$row]['domain'] = array(
	  '#type' => 'textfield',
	  '#default_value' => isset($domain) ? $domain : '',
	  '#size' => 40,
	  '#maxlength' => 255,
	);
	
	$form['settings'][$row]['active'] = array(
	  '#type' => 'checkbox',
	  '#default_value' => isset($active) ? $active : '',
	);

	$row = $row + 1; 
}


function transporter_site_configure_validate($form, &$form_state) {
	foreach ($form_state['values']['settings'] as $row => $values) {
	  /**
	   * @todo set errors if one part of site info is filed in but others are empty.
	   */ 
	}	
}

/**
 * Submit configuration info about the sites.
 */
function transporter_site_configure_form_submit($form, &$form_state) {
	global $base_url;
	$is_master_site = FALSE;
	$master_site = $form_state['values']['master_site'];

	foreach ($form_state['values']['settings'] as $row => $values) {
		if (!empty($values['key'])) {
		  $sites[$row] = $values;
		
		  // Check if this site is the master site. 
			if ($values['key'] == $master_site && $values['domain'] == $base_url) {
        $is_master_site = TRUE; 
			}
			
			if ($values['domain'] == $base_url) {
			  variable_set('transporter_current_site', $values['key']);	
			}
		}
	}
	
  variable_set('transporter_sites', $sites);
  variable_set('transporter_master_site', $form_state['values']['master_site']);
  variable_set('transporter_is_master_site', $is_master_site);
  variable_set('transporter_selector_note', $form_state['values']['selector_note']);
}

/**
 * Callback to set links for a specific page.
 */
function transporter_page_configuration() {

  // Check source path has been set.
	if (isset($_GET['destination'])) {
	  $source_path = $_GET['destination'];	
	}
	else {
	  drupal_goto($path = '');
	}

	$sites = variable_get('transporter_sites', array());
	$current_site_key = variable_get('transporter_current_site', '');
	
  // Get sites list and settings for this page.
	$settings = transporter_paths_get($current_site_key, $source_path);

	$content = drupal_get_form('transporter_page_configuration_form', $sites, $settings, $current_site_key, $source_path);
	return $content;
}

/**
 * Form to set links for a page.
 */
function transporter_page_configuration_form(&$form_state, $sites, $paths, $source_key, $source_path) {
	$form = array();

	$form['settings'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Match Paths'),
	  '#description' => t('Set pages of other sites that this page will link to.'),
	  '#collapsible' => FALSE,
	  '#tree' => TRUE,
	);

	foreach ((array) $sites as $site) {
    $key = $site['key'];

		$form['settings']['paths'][$key] = array(
		  '#type' => 'textfield',
		  '#title' => $site['label'],
		  '#default_value' => isset($paths[$key]['source']) ? $paths[$key]['source'] : '',
		  '#size' => 40,
		  '#maxlength' => 255,
		);
	}
	
	$form['source_key'] = array(
	  '#type' => 'value',
	  '#value' => $source_key,
	);
	
	$form['source_path'] = array(
	  '#type' => 'value',
	  '#value' => $source_path,
	);

	$form['save'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save'),
	);

	return $form;	
}

function transporter_page_configuration_form_submit($form, &$form_state) {
  $paths = $form_state['values']['settings']['paths'];
  $source_key = $form_state['values']['source_key'];
  $source_path = $form_state['values']['source_path'];

	// Save info to database.
	transporter_paths_set($paths, $source_key, $source_path);
}

function theme_transporter_administration_add_paths_form($form) {
	$sites = variable_get('transporter_sites', array());
	$current_site_key = variable_get('transporter_current_site', '');
	
  $header = array();

	foreach ($sites as $site) {
		$header[] = check_plain($site['label']);
	}

	// New paths.
  $rows = array();

  foreach (element_children($form['new_paths']['paths']) as $path) {
    $row = array();

	  foreach ($sites as $site) {	
      $key = $site['key']; 
	    $row[] = drupal_render($form['new_paths']['paths'][$path][$key]);
	  }
			     
		$rows[] = $row; 
  }

  $form['new_paths']['paths']['#value'] = theme('table', $header, $rows);


	$output = drupal_render($form);
  return $output;
}

function theme_transporter_administration_edit_paths_form($form) {
  $rows = array(); 
	$sites = variable_get('transporter_sites', array());
	$current_site_key = variable_get('transporter_current_site', '');
	
  $header = array();

	foreach ($sites as $site) {
		$header[] = check_plain($site['label']);
	}
 
  foreach (element_children($form['paths']) as $path) {
    $row = array();

	  foreach ($sites as $site) {	
      $key = $site['key']; 
	    $row[] = drupal_render($form['paths'][$path][$key]);
	  }
			     
		$rows[] = $row; 
  }

  $form['paths']['#value'] = theme('table', $header, $rows);

	$output = drupal_render($form);
  return $output;
}

function theme_transporter_site_configure_form($form) {
	$sites = variable_get('transporter_sites', array());
	$current_site_key = variable_get('transporter_current_site', '');
	$header = array();
	$rows = array();

	$header = array(t('Key'), t('Label'), t('Domain'), t('Active'));

  foreach (element_children($form['settings']) as $site_row) {
    $row = array();

	  foreach ($form['settings'][$site_row] as $site) {	 
	    $row[] = drupal_render($form['settings'][$site_row]['key']);
	    $row[] = drupal_render($form['settings'][$site_row]['label']);
	    $row[] = drupal_render($form['settings'][$site_row]['domain']);
	    $row[] = drupal_render($form['settings'][$site_row]['active']);
	  } 
			     
		$rows[] = $row; 
  }

  $form['settings']['#value'] = theme('table', $header, $rows);

	$output = drupal_render($form);
  return $output;
}

 